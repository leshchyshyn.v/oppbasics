<?php
ini_set('include_path', __DIR__);

ini_set('display_errors', 1);
error_reporting(E_ALL);

include_once 'vendor/autoload.php';

//include('App/Autoload.php');


$rectangle = new \App\Code\Geometry\Rectangle();

$rectangle->setA(15)->setB(8);

echo $rectangle->getA()."\n";
echo $rectangle->getB()."\n";


try {
    echo \App\Code\Geometry\Rectangle::getStaticSquare();
} catch (\App\Fixtures\SideNotSetException $e) {
    $log = new \Katzgrau\KLogger\Logger('var/log');
    $log->error($e->getMessage());
}