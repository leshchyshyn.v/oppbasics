<?php

namespace App\Code\Api;

interface SquareOperationInterface
{
    public function getSquare();
}