<?php

namespace App\Code\Geometry;

use \Katzgrau\KLogger\Logger;

abstract class Figure
{
    const M_PI = 3.14;

    public $a; //side A

    protected $logger;

    public function __construct($a = null)
    {
        $this->logger = new Logger("var/log");
        echo "Figure constructor \n";
        if ($a) {
            $this->a = $a;
        }
    }

    public function __call($name, $arguments)
    {
        $this->logger->info("Class: ".self::class);
        $this->logger->info("Called method: ".$name);
        if (substr($name,0,3)=="get")
        {
            $var = strtolower(substr($name,3));
            if ($this->$var) {
                return $this->$var;
            }
        }
        if ((substr($name,0,3)=="set")&& isset($arguments[0]))
        {
            $var = strtolower(substr($name,3));
            $this->$var = $arguments[0];
            return $this;
        }
    }
}