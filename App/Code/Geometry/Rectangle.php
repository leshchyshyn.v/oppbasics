<?php

namespace App\Code\Geometry;

class Rectangle extends Square
{
    public $b;

    public function __construct($a = null, $b = null)
    {
        if ($b) {
            $this->b = $b;
        }
        parent::__construct($a);
    }

    public function printA()
    {
        echo "This is A: ".$this->a;
    }

    public function getPerimeter()
    {
        return $this->getA()*2 + $this->getB()*2;
    }

    public function getSquare()
    {
        return $this->getA()*$this->getB();
    }
}